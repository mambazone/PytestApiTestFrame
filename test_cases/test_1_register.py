import json
import unittest
from unittestreport import ddt, list_data, json_data, yaml_data


from tools.handle_replace import HandleReplace
from tools.handle_request import HandleRequest
from tools.handle_excel import HandleExcel
from tools.handle_path import case_data_dir
from tools.handle_response import HandleResponse

# 从excel中获取测试数据
case_list = HandleExcel(file_name=case_data_dir, sheet_name="register").get_cases()


@ddt
class Register(unittest.TestCase):
    @classmethod
    def setUp(cls) -> None:
        cls.replace = HandleReplace()
        cls.response = HandleResponse()
        cls.request = HandleRequest()

    # @data(*case_list)
    @list_data(case_list)
    def test_register(self, case):
        """
        title: 注册账号
        :param case:
        :return:
        """
        print(type(case), case)
        request_data = self.replace.replace_data(data=case["data"])
        resp = self.request.send_request(
            is_upload=case["is_upload"],
            method=case["method"],
            url=case["url"],
            data=request_data,
            port=case["port"],
        )

        print("请求参数的数据类型：", type(case["data"]))
        print("响应结果：", type(resp), resp)
        # 响应结果处理
        # new_response = self.response.handle_response(response=resp)
        # print(new_response)
        # 接口断言
        self.response.assert_response(response=resp, expected_data=case["expected_data"])
        # 提取token，设置为类属性
        # token = jsonpath(new_response, "$..token")[0]


if __name__ == '__main__':
    unittest.main()
