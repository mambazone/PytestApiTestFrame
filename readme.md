## 目录结构

```
│  debug.py
│  list.txt
│  main.py
│  readme.md
│  test.py
│
├─conf
│  │  settings.py
│  │  __init__.py
│
├─image
│      1.jpg
│      __init__.py
│      
├─logs
│      20240228.log
│      __init__.py
│      
├─reports
│      20230728_144618.html
│      history.json
│      __init__.py
│      
├─test_cases
│  │  test_1_login.py
│  │  test_2_upload.py
│  │  test_3_normal_order.py
│  │  __init__.py
│
├─test_datas
│      case_data.xlsx
│      __init__.py
└─tools
    │  __init__.py
    │  handle_assert_db.py
    │  handle_attribute.py
    │  handle_data.py
    │  handle_excel.py
    │  handle_excel_all.py
    │  handle_extract_data.py
    │  handle_logs.py
    │  handle_oracle.py
    │  handle_path.py
    │  handle_replace.py
    │  handle_request.py
    │  handle_response.py
    │  handle_setup_sql.py
```

#### api

调试接口用，可忽略

#### conf：存放配置信息（数据库连接信息、测试站点等）

，需要使用的时候，直接导入调用，比如封装数据库工具类`handle_oracle`的时候需要调用`conf`中的数据库信息：

```python
import cx_Oracle
from conf.settings import oracle_info

class HandleOracle:
    def __init__(self):
        self.db = cx_Oracle.connect(
            oracle_info["name"],
            oracle_info["pwd"],
            oracle_info["host"],
        )
        self.cur = self.db.cursor()
```
#### image：存放图片

当某个接口需要上传图片附件，需要使用的图片放在这里

#### logs：存放日志

项目运行生成的日志，存放在这里

reports：存放测试报告，history.json中存放的是历史数据，因为报告中包含每次执行测试用例的结果趋势，所以可以酌情删除

test_cases：存放测试用例

test_datas：存放测试数据，测试用例读取test_datas中的数据执行

tools：封装的操作方法，主要包括：断言方法、属性设置、测试数据操作方法、Excel文档操作方法、提取数据方法、日志生成、oracle数据库操作方法、路径设置、数据替换、接口请求方法、响应报文处理方法、前置sql处理

debug.py：调试用，可忽略

start.py：执行
