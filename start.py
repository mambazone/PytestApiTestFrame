import time
import unittest
import unittestreport
from tools.handle_attribute import HandleAttr
from test_cases.test_all import generator_case_class
from tools.handle_excel_all import HandleExcelAll
from tools.handle_move_file import HandleMoveFile
from tools.handle_path import case_data_dir
from tools.handle_path import file_name, report_dir
# from tools.handle_mysql import mysql


class Start:
    def __init__(self):
        self.excel = HandleExcelAll(file_name=case_data_dir)

    # 数据清洗
    def __del_attribute(self):
        print("HandleAttr属性", HandleAttr.__dict__)
        attr_name = list(vars(HandleAttr).keys())
        for name in attr_name:
            if not name.startswith("_"):
                delattr(HandleAttr, name)
            else:
                print("下划线开头的私有属性不删除")

    def start(self):
        # 读取excel数据（通过sheet表名）
        sheet_name_list = self.excel.wb.sheetnames
        # 挪走历史测试报告
        HandleMoveFile.move_file()
        for sheet_name in sheet_name_list:
            # 执行之前先清洗数据
            self.__del_attribute()
            # 获取sheet数据
            data_list = self.excel.get_all_cases(sheet_name=sheet_name)
            # 用获取到的数据做数据驱动，动态生成测试用例
            case_class = generator_case_class(case_list=data_list)

            # 收集测试用例
            suite = unittest.defaultTestLoader.loadTestsFromTestCase(case_class)
            # 测试报告文件名称
            filename = time.strftime("%Y%m%d_%H%M%S", time.localtime())
            # filename = str(int(time.time()))
            # 执行测试用例，生成测试，发送测试报告到邮件
            runner = unittestreport.TestRunner(
                suite=suite,
                filename=f"{sheet_name}_{filename}.html",
                report_dir=report_dir,
                title=sheet_name,
                tester="Sean",
                desc=f"{sheet_name}接口自动化测试",
                templates=2
            )
            runner.run()
            # 发送邮件

            # mysql.close_db()
            # self.excel.wb.close()


if __name__ == "__main__":
    cl = Start()
    cl.start()

