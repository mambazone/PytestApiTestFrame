import re
import time
import uuid
import json

from tools.handle_data import HandleData
from tools.handle_attribute import HandleAttr
from tools.handle_logs import my_log


class HandleReplace:

    def __init__(self):
        self.data = HandleData()

    def replace_data(self, data: str):
        my_log.info(msg="开始替换数据")
        """
        1、获取需要替换的数据
        2、遍历需要替换的数据，根据对应的数据生成对应的值
        """
        if data:
            key_list = re.findall("#(\w.+?)#", data)  # 使用正则表达式分组匹配
            my_log.info(msg=f"需要被替换的参数：{key_list}")
            print("需要被替换的参数：", key_list)
            if len(key_list) > 0:
                # 生成需要替换的数据（时间戳和uuid等）
                for key in key_list:
                    if key == "time":
                        times = str(int(time.time() * 1000))  # 生成时间戳
                        setattr(HandleAttr, key, times)  # 将生成的数据设置为HandleAttr类的属性
                    elif key == "sessionUUID":
                        session_uuid = str(uuid.uuid4())  # 生成UUID
                        setattr(HandleAttr, key, session_uuid)
                    elif key == "filepath":
                        filepath = str(getattr(HandleAttr, "objectName"))
                        setattr(HandleAttr, key, filepath)
                    elif key == "orderNumbers":
                        orderNumbers = str(getattr(HandleAttr, "orderNumber"))
                        setattr(HandleAttr, key, orderNumbers)
                    elif key == "name":
                        name = self.data.get_name()
                        setattr(HandleAttr, key, name)
                    elif key == "mobile":
                        mobile = self.data.get_mobile()
                        setattr(HandleAttr, key, mobile)
                    elif key == "address":
                        address = self.data.get_address()
                        setattr(HandleAttr, key, address)
                # 替换数据
                for key in key_list:
                    data = data.replace(f"#{key}#", getattr(HandleAttr, key))
                # 将替换后的数据转换为json对象
                data = json.loads(data)
                my_log.info(msg=f"替换请求参数后，返回json数据：{data}")
                # print(f"替换后的参数：", data)
                return data
            else:
                try:
                    print("不需要替换请求参数，直接返回json数据")
                    data = json.loads(data)  # 兼容data不为空但是不需要替换的场景
                except:
                    data = data  # 兼容data为空不需要请求参数的场景
                return data
        else:
            print("请求参数为空，不需要替换参数")
            return {}

    # 替换sql语句
    def replace_sql(self, sql: str):
        my_log.info(msg="开始替换sql")
        if sql:
            key_list = re.findall("#(\w.+?)#", sql)
            # 获取需要替换的sql语句，默认从全局变量中获取，暂时不拓展
            # 替换sql语句
            if len(key_list) > 0:
                my_log.info(msg="sql语句需要替换")
                for key in key_list:
                    my_log.info(msg=f"替换之前的sql：{sql}")
                    sql = sql.replace(f"#{key}#", getattr(HandleAttr, key))
                my_log.info(msg=f"替换完成，替换之后的sql：{sql}")
            else:
                my_log.info(msg="当前sql语句不需要被替换")
                # print("当前sql语句不需要被替换")
        else:
            my_log.info(msg="搜索sql语句为空，不需要被替换")
            # print("搜索sql语句为空，不需要替换")
        return sql
