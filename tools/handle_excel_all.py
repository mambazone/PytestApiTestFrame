from openpyxl import load_workbook


class HandleExcelAll:
    # 初始化
    def __init__(self, file_name):
        # 创建表对象
        self.wb = load_workbook(filename=file_name)

    # 获取测试用例
    def get_all_cases(self, sheet_name):
        case_list = []
        sheet_obj = self.wb[sheet_name]
        cases = list(sheet_obj.iter_rows(values_only=True))
        title = cases[0]  # 获取表头
        values = cases[1:]  # 获取表数据
        for case in values:
            case_dict = dict(list(zip(title, case)))  # 打包表数据和表头，变成一个dict
            case_list.append(case_dict)  # 将每一个测试用例追加到list中，集中返回
        # self.close_file()
        return case_list
