# @Time    : 2024/5/4 2:52
# @Author  : Jason Sean
# @File    : handle_mysql.py
# @Software: PyCharm
# @Project_Name: ApiTestFrame
import pymysql
from conf.settings import mysql_info


class HandleMysql:
    def __init__(self):

        self.db = pymysql.connect(
            host=mysql_info["host"],
            port=mysql_info["port"],
            user=mysql_info["user"],
            password=mysql_info["password"],
            db=mysql_info["db"],
            autocommit=True,
            charset="utf8",
            cursorclass=pymysql.cursors.DictCursor
        )

        self.cur = self.db.cursor()

    # 关闭数据库连接
    def close_db(self):
        self.cur.close()
        self.db.close()

    # 查询数据，返回元组
    def get_data(self, sql):
        value_list = []
        self.cur.execute(sql)
        result = self.cur.fetchall()
        for dict_data in result:
            for value in dict_data.values():
                value_list.append(value)
        return value_list

    # 查询数据，返回字典
    def get_dict_data(self, sql):
        self.cur.execute(sql)
        result = self.cur.fetchall()
        return result


# 单例
mysql = HandleMysql()
