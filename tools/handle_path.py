import os
import time

# 动态存储需要使用的各种文件路径

# 项目根路径
base_path = os.path.dirname(os.path.dirname(__file__))
print(base_path)

# 测试数据路径
case_data_dir = os.path.join(base_path, "test_datas", "case_data.xlsx")
print(case_data_dir)

# 测试用例路径
case_dir = os.path.join(base_path, "test_cases")
# 日志路径
log_name = time.strftime("%Y%m%d", time.localtime())
log_dir = os.path.join(base_path, "logs", f"{log_name}.log")

# 测试报告路径
file_name = time.strftime("%Y%m%d_%H%M%S", time.localtime())
report_dir = os.path.join(base_path, "reports")
history_dir = os.path.join(report_dir, "history")
print(history_dir)
# 图片路径
img_dir = os.path.join(base_path, "image", "1.jpg")
print(img_dir)
