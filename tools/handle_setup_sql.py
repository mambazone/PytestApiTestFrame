"""
前置sql语句处理
"""
import json
# import ast
from tools.handle_logs import my_log

from tools.handle_attribute import HandleAttr
from tools.handle_mysql import HandleMysql
from tools.handle_replace import HandleReplace


# ast.literal_eval()


class HandleSetupSql:

    def __init__(self):
        self.replace = HandleReplace()
        self.mysql = HandleMysql()

    # [select mobile_code from tz_sms_log where user_phone='#mobile#' order by id desc limit 1", ""]
    def setup_sql(self, sql_list: str):
        my_log.info(msg="开始执行前置sql")
        my_log.info(msg=f"入参数据类型：{type(sql_list)}，参数：{sql_list}")
        try:
            if sql_list:
                my_log.info(msg="sql_list不为空，需要执行前置sql语句")
                sql_list = sql_list if isinstance(sql_list, list) else json.loads(sql_list)
                for sql in sql_list:
                    # 替换sql语句
                    self.replace.replace_sql(sql=sql)
                    # 执行sql语句
                    result = self.mysql.get_data(sql=sql)
                    print(result)
                    # 设置为类属性
                    if len(result) > 0:
                        # 遍历查询结果，有可能有多行数据，所以这里做一个兼容处理
                        for dict_data in result:
                            # 遍历list的每一个结果（单行数据的所有结果）
                            for key, value in dict_data.items():
                                # 设置属性
                                setattr(HandleAttr, key, value)
                    else:
                        my_log.info(msg="前置sql语句执行后，返回的结果为空，不需要设置属性")
            else:
                my_log.info(msg="sql_list为空,不需要执行前置sql语句获取数据")

        except Exception as e:
            my_log.exception(msg=e)
            raise TypeError
