from faker import Faker


class HandleData:
    def __init__(self):
        self.fake = Faker(locale="zh_CN")

    def get_mobile(self):
        return self.fake.phone_number()

    def get_email(self):
        return self.fake.email()

    def get_name(self):
        return self.fake.name()

    def get_address(self):
        return self.fake.address()

    def get_city(self):
        return self.fake.city()

    def get_province(self):
        return self.fake.province()

    def get_country(self):
        return self.fake.country()

    def get_postcode(self):
        return self.fake.postcode()

    def get_password(self):
        return self.fake.password()

    def get_url(self):
        return self.fake.url()

    def get_date(self):
        return self.fake.date()

    def get_datetime(self):
        return self.fake.datetime()


if __name__ == '__main__':
    print(HandleData().get_mobile())
    print(HandleData().get_email())
    print(HandleData().get_name())
