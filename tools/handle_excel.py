from openpyxl import load_workbook
from pprint import pprint


class HandleExcel:
    # 初始化
    def __init__(self, file_name, sheet_name):
        # 创建表对象
        self.wb = load_workbook(filename=file_name)
        # 创建sheet对象
        self.sheet_obj = self.wb[sheet_name]

    # 获取测试用例
    def get_cases(self):
        case_list = []
        cases = list(self.sheet_obj.iter_rows(values_only=True))
        title = cases[0]
        values = cases[1:]
        for case in values:
            case_dict = dict(list(zip(title, case)))
            case_list.append(case_dict)
        self.close_file()
        return case_list

    def close_file(self):
        self.wb.close()


if __name__ == '__main__':
    cl = HandleExcel(file_name="D:/files/PycharmProject/ApiTestFrame/test_datas/case_data.xlsx", sheet_name="register")
    result = cl.get_cases()
    pprint(result)
