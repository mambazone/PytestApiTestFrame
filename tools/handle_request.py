import re

import requests
from requests_toolbelt import MultipartEncoder
from tools.handle_attribute import HandleAttr
from tools.handle_path import img_dir
from tools.handle_response import HandleResponse
from conf.settings import host_info


class HandleRequest:
    """
    1、获取登录的token，设置到请求头
    2、兼容普通的请求，扼要兼容图片上传的请求
    """

    def __init__(self):
        self.headers = {"locale": "zh_CN", "X-Lemonban-Media-Type": "lemonban.v2"}
        self.response = HandleResponse()

    def __handle_url(self, url: str, port: str):
        if url.startswith("http"):
            print("全路径，不需要拼接url")
        else:
            url = host_info["host"] + str(port) + url
            print("需要拼接url")
        key_list = re.findall("#(\w.+?)#", url)
        if len(key_list) > 0:
            for key in key_list:
                if hasattr(HandleAttr, key):
                    url = url.replace(f"#{key}#", getattr(HandleAttr, key))
                else:
                    print(f"{key}在全局变量中不存在，无法替换url参数")
        else:
            print("url中没有需要替换的参数")
        return url

    def __handle_headers(self):
        if hasattr(HandleAttr, "token"):
            token = getattr(HandleAttr, "token")
            # headers["Authorization"] = token
            self.headers["Authorization"] = f"Bearer {token}"
        else:
            print("全局属性中没有token属性，该接口不需要鉴权")

    # 图片上传的请求
    def __upload_file(self, url):
        try:
            with open(file=img_dir, mode="rb") as file:
                image = file.read()
                data = MultipartEncoder(
                    fields={
                        "file": ("1.jpg", image, "image/jpeg")
                    }
                )
                self.headers["Content-Type"] = data.content_type
                print(self.headers)
                resp = requests.post(url=url, data=data, headers=self.headers)
                print("图片上传响应：", resp.text)
                return resp
        except Exception as e:
            print("图片上传错误：", e)
            return {}
        finally:
            self.headers["Content-Type"] = "application/json"

    # 发送请求
    def send_request(self, is_upload, method, data, port, url):
        # 鉴权处理
        self.__handle_headers()
        # url处理
        url = self.__handle_url(url=url, port=port)
        if is_upload == 1:
            # 图片上传的请求
            print("图片上传的接口")
            resp = self.__upload_file(url=url)
            print("图片上传接口响应结果：", type(resp), resp)
        else:
            print("普通请求")
            # 普通接口发送请求
            resp = requests.request(method=method, url=url, json=data, headers=self.headers)
            print("普通接口响应结果：", type(resp), resp)
        resp = self.response.handle_response(response=resp)
        return resp
