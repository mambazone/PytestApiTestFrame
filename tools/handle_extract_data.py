import json
from jsonpath import jsonpath

from tools.handle_attribute import HandleAttr
from tools.handle_logs import my_log


class HandleExtractData:
    def __init__(self):
        pass

    # extract_data就是测试用例Excel中的字段，extract_data = {"token": "$..token"}
    # response = {"token": "573892573294085732907538295"}
    def extract_data(self, response: dict, extract_data: str):
        my_log.info(msg="开始提取数据")
        if extract_data:
            extract_data = extract_data if isinstance(extract_data, dict) else json.loads(extract_data)
            my_log.info(msg=f"待提取的数据为{extract_data}")
            for key, val in extract_data.items():
                # 将token提取出来
                value = jsonpath(response, val)[0]
                my_log.info(msg=f"提取出来的数据：{key}：{value}")
                print(f"提取出来的数据：{key}：", value)
                setattr(HandleAttr, key, value)  # 将token设置为类属性

        else:
            my_log.info(msg="Excel中extract_data为空，不需要提取数据")
            # print("Excel中该字段为空，无需提取数据")


if __name__ == '__main__':
    re = {'code': 0, 'msg': 'OK', 'data': {'id': 193921, 'leave_amount': 5000.0, 'mobile_phone': '13831055870', 'reg_name': '蔡秀珍', 'reg_time': '2024-05-04 03:15:33.0', 'type': 1, 'token_info': {'token_type': 'Bearer', 'expires_in': '2024-05-04 16:22:11', 'token': 'eyJhbGciOiJIUzUxMiJ9.eyJtZW1iZXJfaWQiOjE5MzkyMSwiZXhwIjoxNzE0ODEwOTMxfQ.-89LzEh0BtnRCpyMvrUL9UD7KDMN3sg6h5R5BDZaZgM-SP-xa-FzE-NJ5w-BDXfjeCY-0voZc-7B1LLqqz-OTA'}}, 'copyright': 'Copyright 柠檬班 © 2017-2020 湖南省零檬信息技术有限公司 All Rights Reserved'}
    ed = {'token': '$..token'}
    cl = HandleExtractData()
    cl.extract_data(response=re, extract_data=ed)
