import json
from jsonpath import jsonpath
from unittest import TestCase


class HandleResponse:
    def __init__(self):
        self.my_assert = TestCase()

    # 响应结果处理
    def handle_response(self, response):
        # print("handle_response接收到的response：", type(response))
        """
        1、有参数接收响应结果
        2、根据数据类型判断是否需要二次封装
        """
        try:
            if isinstance(response.json(), dict):
                # 返回response.json()
                return {"response": response.json()}
        except Exception as e:
            print("执行报错了，错误信息：", e)
            # 执行报错了，response.text，二次封装成json
            return {"response": response.text}

    # 接口断言
    def assert_response(self, response, expected_data):
        """
        期望结果==我们事先设置好的结果，比如{"code":200,"message":"成功"}
        实际结果 == 接口执行返回的结果
        1、从Excel读取期望结果的值
        2、遍历期望结果的key，通过key从响应结果中提取对应的value
        3、断言期望和实际结果
        4、准入条件：expected_data不为空
        """
        if expected_data:
            # 创建一个空字典存放实际结果
            actual_data = {}
            # 将期望结果转换为python对象
            expected_data = expected_data if isinstance(expected_data, dict) else json.loads(expected_data)
            # print("expected_data：", type(expected_data), expected_data)
            for key in expected_data.keys():
                # 从响应结果中提取对应的value，取出来的是一个list
                # print(jsonpath(response, f"$..{key}"))
                actual_data[key] = jsonpath(response, f"$..{key}")[0]
                # print("actual_data：", type(actual_data), actual_data)

            # 断言期望结果和实际结果
            self.my_assert.assertEqual(expected_data, actual_data)
            print("断言通过")
        else:
            print("Excel中该字段为空，无需断言。")

