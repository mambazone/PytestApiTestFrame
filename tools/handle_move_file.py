# @Time    : 2024/5/4 18:37
# @Author  : Jason Sean
# @File    : handle_move_file.py
# @Software: PyCharm
# @Project_Name: ApiTestFrame
import shutil
import os
from tools.handle_path import report_dir, history_dir


class HandleMoveFile:
    @classmethod
    def move_file(cls):
        name_list = os.listdir(report_dir)
        print("reports目录下的文件名称和目录名称", name_list)
        for name in name_list:
            if name.endswith(".html"):
                src = os.path.join(report_dir, name)
                # 挪走文件
                shutil.move(src, history_dir)
                print("html文件挪动成功")
            else:
                print("非测试报告文件无需挪动")


if __name__ == '__main__':
    HandleMoveFile.move_file()
