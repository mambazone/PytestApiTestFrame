import json
from unittest import TestCase
from tools.handle_replace import HandleReplace
from tools.handle_oracle import oracle
from tools.handle_logs import my_log


class HandleAssertOracle:
    def __init__(self):
        self.replace = HandleReplace()
        self.my_assert = TestCase()

    def assert_oracle(self, assert_db):
        try:
            if assert_db:
                # 转换数据类型，将excel中的数据库断言数据转成dict
                assert_db = assert_db if isinstance(assert_db, dict) else json.loads(assert_db)
                # 获取实际结果的sql语句
                sql = assert_db["actual_data"]
                # 检查是否需要替换sql语句，如果需要则替换
                self.replace.replace_sql(sql=sql)
                # 执行sql语句
                result = oracle.get_data(sql=sql)
                # 断言，注意assertEqual()没有任何返回，不需要接收返回值
                self.my_assert.assertEqual(assert_db["expected_data"], result[0][0])
                my_log.info(msg="数据库断言成功")
                # print("数据库断言成功")
            else:
                my_log.info(msg="Excel中assert_db字段为空，不需要断言")
                # print("Excel中assert_db字段为空，不需要断言")
        except Exception as e:
            my_log.exception(msg=e)
            raise TypeError
            # print("assert_db报错了", e)
        finally:
            oracle.close_db()





