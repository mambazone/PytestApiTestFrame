import logging
from logging import handlers

from tools.handle_path import log_dir


def my_log():
    # 创建日志收集器
    api_test = logging.getLogger(name="api_test")
    # 创建日志输出渠道
    pycharm = logging.StreamHandler()
    # 文件渠道
    log_file = handlers.TimedRotatingFileHandler(
        filename=log_dir,
        interval=1,
        encoding="utf-8",
        when="D",
        backupCount=20
    )
    # 日志格式
    ft = ">>>%(asctime)s-%(name)s-%(levelname)s-%(message)s"
    log_style = logging.Formatter(fmt=ft)

    # 日志绑定
    pycharm.setFormatter(fmt=log_style)
    log_file.setFormatter(fmt=log_style)

    # 设置日志级别
    api_test.setLevel(level=logging.DEBUG)

    # 将日志输出渠道绑定到日志收集器上
    api_test.addHandler(pycharm)
    api_test.addHandler(log_file)
    return api_test


my_log = my_log()
