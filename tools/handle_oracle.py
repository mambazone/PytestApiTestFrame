import cx_Oracle
from conf.settings import oracle_info


class HandleOracle:
    def __init__(self):
        self.db = cx_Oracle.connect(
            oracle_info["name"],
            oracle_info["pwd"],
            oracle_info["host"],
        )
        self.cur = self.db.cursor()

    def close_db(self):
        self.cur.close()
        self.db.close()

    def get_data(self, sql):
        """
        查询数据，返回列表
        :param sql:
        :return:
        """
        value_list = []
        self.cur.execute(sql)
        result = self.cur.fetchall()
        print(type(result), result)
        for dict_data in result:
            if isinstance(dict_data, dict):
                for value in dict_data.values():
                    value_list.append(value)
            else:
                print("查询结果不包含字典类型的数据")
        return value_list

    def get_default_data(self, sql):
        self.cur.execute(sql)
        result = self.cur.fetchall()
        print(type(result), result)
        return result


# 单例
oracle = HandleOracle()

if __name__ == "__main__":
    sql_query = "select count(*) from DC_ENUM_VALUES where TYPE_NAME='invoiceNote'"
    cl = HandleOracle()
    # 查询数据，返回元组
    res_tuple = cl.get_data(sql=sql_query)
    # 查询数据，返回字典
    res_dict = cl.get_default_data(sql=sql_query)
    cl.close_db()
