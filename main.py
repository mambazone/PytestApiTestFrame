import unittest
import unittestreport

from tools.handle_path import case_dir, file_name, report_dir

suite = unittest.defaultTestLoader.discover(start_dir=case_dir, pattern="test_1_register.py")

runner = unittestreport.TestRunner(
    suite=suite,
    filename="{}.html".format(file_name),  # 兼容python3.6以下的版本
    report_dir=report_dir,
    title="注册账号",
    tester="Sean",
    desc="测试注册成功流程",
    templates=2
)
runner.run()

"""
发送邮件
runner.send_email(
    host="smtp.qq.com",
    port=465,
    password="test_name@qq.com",
)
"""