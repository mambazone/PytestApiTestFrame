import requests
from jsonpath import jsonpath

# 用户登录
url = "**********"
data = {
    "loginName": "****",
    "password": "1"
}
res = requests.post(url=url, json=data)
if res.status_code == 200:
    print("登录成功")
# print(res.json())
log_token = jsonpath(res.json(), "$..token")[0]
# print(log_token)

# 获取初始化菜单
url2 = "***********"
res2 = requests.get(url=url2, headers={
    # "Authorization": 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjdXJyZW50VGltZU1pbGxpcyI6IjE2ODg1NzUzMjc5MDEiLCJleHAiOjE2ODg2NjE3MjcsInVzZXJJZCI6IjEyMzU2NiJ9.yB0yzeVV8q1kdAwIrWGiHA4eC6tLiPQQmuervsTqEQU'})
    "Authorization": log_token})
if res2.status_code == 200:
    print("获取初始化菜单成功")
