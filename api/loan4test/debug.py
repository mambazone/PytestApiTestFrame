# @Time    : 2024/4/11 23:50
# @Author  : Jason Sean
# @File    : debug.py
# @Software: PyCharm
# @Project_Name: ApiTestLoan
import requests
from api.loan4test.data import register_data
from api.loan4test.data import login_data

class Loan_Requests:
    def __init__(self, url, method, headers, data):
        self.url = url
        self.method = method
        self.headers = headers
        self.data = data

    def register(self):
        res = requests.request(url=self.url, method=self.method, headers=self.headers, json=self.data)
        print(register_data)
        return res.json()

    def login(self):
        res = requests.request(url=self.url, method=self.method, headers=self.headers, json=self.data)
        print(register_data)
        return res.json()


if __name__ == '__main__':
    # register_user = Loan_Requests(url=register_data['url'], method=register_data['method'], headers=register_data['headers'], data=register_data['data'])
    login_user = Loan_Requests(url=login_data['url'], method=login_data['method'], headers=login_data['headers'], data=login_data['data'])
    print(login_user.login())

