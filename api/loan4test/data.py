# @Time    : 2024/4/11 23:54
# @Author  : Jason Sean
# @File    : data.py
# @Software: PyCharm
# @Project_Name: ApiTestLoan
register_data = {
    "url": "http://api.lemonban.com/futureloan/member/register",
    "method": "post",
    "headers": {"X-Lemonban-Media-Type": "lemonban.v1", "Content-Type": "application/json"},
    "data": {"mobile_phone": "18877887788", "pwd": "88888888"}
}
login_data = {
    "url": "http://api.lemonban.com/futureloan/member/login",
    "method": "post",
    # "headers": {"X-Lemonban-Media-Type": "lemonban.v2", "Content-Type": "application/json"},
    "headers": {"X-Lemonban-Media-Type": "lemonban.v2", "Content-Type": "application/json"},
    "data": {"mobile_phone": "18877887788", "pwd": "88888888"}
}
