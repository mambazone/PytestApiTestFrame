"""
存放配置信息，存放静态的，需要手动输入的环境配置等信息
"""
oracle_info = {
    "name": "bv",
    "host": "10.0.1.58/ebai",
    "pwd": "bvdev123321"
}
host_info = {
    "host": "http://10.0.3.25:",
}

mysql_info = {
    "name": "future",
    "host": "api.lemonban.com",
    "pwd": "123456",
    "port": "3306",
}